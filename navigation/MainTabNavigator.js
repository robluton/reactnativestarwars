import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import ResultsScreen from '../screens/ResultsScreen';
import SearchScreen from '../screens/SearchScreen';
import DetailsScreen from '../screens/DetailsScreen';
import { text, textColor } from '../styles';
import { colors } from '../theme';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const RootStack = createStackNavigator(
  {
    Results: {
      screen: ResultsScreen,
    },
    Search: {
      screen: SearchScreen,
    },
    Details: {
      screen: DetailsScreen,
    }
  },
  {
    initialRouteName: 'Search',
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: {
        borderBottomColor: colors.primary,
      },
      headerTintColor: colors.primary,
      headerTitleStyle: {
        fontWeight: 'bold',
        ...text.regular,
        ...textColor.primary,
      },
    },
  }
);

export default RootStack;
