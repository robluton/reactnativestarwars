import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import Button from './Button';
import { text, textColor } from '../styles';

const MovieList = class FlatListBasics extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { movieList, navigation } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={movieList}
          renderItem={({item}) => (
            <View style={styles.item}>
              <Text style={text.regular, textColor.medium, { marginBottom: 10 } }>{item.name}</Text>
              <Button onPress={() => navigation.navigate('Details', { details: item })}>SEE DETAILS</Button>
            </View>
          )
        }
        />
      </View>
    );
  }
}

export default withNavigation(MovieList);

const styles = StyleSheet.create({
  container: {
   flex: 1,
  },
  item: {
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    paddingVertical: 20,
  }
})