import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { colors } from '../theme';

export default function Button({ children = 'SUBMIT', onPress, disabled = false }) {
  return (
    <TouchableOpacity
        style={styles.ButtonStyle}
        activeOpacity = { .5 }
        onPress={onPress}
        disabled={disabled}
    >
      <Text style={styles.TextStyle}>{children}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  ButtonStyle: {
    paddingTop:5,
    paddingBottom:5,
    backgroundColor: colors.primary,
    borderRadius:30,
  },

  TextStyle:{
      color:'#fff',
      textAlign:'center',
  }
});