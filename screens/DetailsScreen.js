import React from 'react';
import { ScrollView, Text } from 'react-native';
import { layout, text } from '../styles';

const excludedFromDisplay = ['key'];

export default function DetailsScreen({ navigation }) {
  const details = navigation.getParam('details', '');
  const content = Object.entries(details).reduce((result, entry) => {
    const item = (
    <Text key={entry[0]} style={{ flex: 1 }, text.regular}>{`
      ${entry[0]}: ${entry[1]}
      `}
    </Text>
    );
    if (!excludedFromDisplay.includes(entry[0])) {
      result.push(item)
    }
    return result;
  }, []);
  return (
    <ScrollView style={layout.container}>
      <Text style={text.semiBold}>Place Holder Page - Not Finished</Text>
      <Text style={text.regular}>{content}</Text>
    </ScrollView>
  );
}

DetailsScreen.navigationOptions = {
  title: 'Details',
};
