import React, { useState } from 'react';
import { View, TextInput, StyleSheet, Text } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import Button from '../components/Button';
import { layout, text, textColor } from '../styles';
import { colors } from '../theme';

const radioProps = [
  {label: 'People', value: 'people' },
  {label: 'Movies', value: 'movies' },
];

function RadioButtons({ onPress }) {
  return (
    <View>
      <RadioForm
        radio_props={radioProps}
        initial={0}
        formHorizontal={true}
        labelHorizontal={true}
        buttonInnerColor={colors.primary}
        buttonColor={'#ccc'}
        buttonSize={10}
        buttonOuterSize={20}
        onPress={onPress}
        labelStyle={{ marginRight: 20 }}
      />
    </View>
  );
}

const placeholderLookup = {
  people: 'e.g. Chewbacca, Yoda',
  movies: 'e.g., Return of the Jedi',
}

export default function SearchScreen({ navigation }) {
  const [searchType, setSearchType] = useState(radioProps[0].value);
  const [searchQuery, setSearchQuery] = useState('');
  const placeholderText = placeholderLookup[searchType];
  const searchEnabled = searchType && searchQuery;
  return (
    <View style={layout.container}>
      <Text style={{ ...text.medium, ...textColor.light, ...styles.text}}>What are you searching for ?</Text>
      <RadioButtons onPress={(value) => setSearchType(value)} />
      <TextInput
        onChangeText={(text) => setSearchQuery(text)}
        value={searchQuery}
        placeholder={placeholderText}
        borderWidth={1}
        borderColor={'#ccc'}
        style={styles.input}
      />
      <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 40 }}>
        <Button disabled={!searchEnabled} onPress={() => navigation.navigate('Results', { searchType, searchQuery })}>SEARCH</Button>
      </View>
    </View>
  );
}

SearchScreen.navigationOptions = {
  title: 'SWStarter',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 30,
    backgroundColor: '#fff',
  },
  input: {
    height: 40,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  text: {
    marginBottom: 20,
  }
});
