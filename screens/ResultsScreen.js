import * as WebBrowser from 'expo-web-browser';
import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
} from 'react-native';

import { layout, text, textColor } from '../styles';
import CustomButton from '../components/Button';
import MovieList from '../components/MovieList';
import { colors } from '../theme';

const endpointLookup = {
  movies: 'films',
  people: 'people',
};

const normalizers = {
  movies: movies => movies.map(movie => ({
    name: movie.title,
    openingCrawl: movie.opening_crawl,
    key: movie.episode_id,

  })),
  people: people => people.map(person => ({
    name: person.name,
    key: `${person.name}-${person.birth_year}`,
    height: person.height,
    mass: person.mass,
    hairColor: person.hair_color,
    birthYear: person.birth_year,
    gender: person.gender,
  })),
};

export default function ResultsScreen({ navigation }) {
  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const searchType = navigation.getParam('searchType', 'movies');
    const searchQuery = navigation.getParam('searchQuery', '');
    const endpoint = endpointLookup[searchType];
    async function getDataFromApi() {
      try {
        setIsLoading(true)
        let response = await fetch(
          `https://swapi.co/api/${endpoint}/?search=${searchQuery}`,
        );
        let responseJson = await response.json();
        const normalizedData = normalizers[searchType](responseJson.results);
        setSearchResults(normalizedData);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
      }
    }
    getDataFromApi();
  }, []);

  return (
    <View style={layout.container}>
      <ScrollView>
        <View>
          { isLoading && (<Text style={{...textColor.light, ...text.regular}}>Searching...</Text>) }
          <Text style={{ ...text.semiBold, ...textColor.medium }, { fontSize: 18 }}>Results</Text>
          { !isLoading && !searchResults.length && (
            <Text style={styles.zeroStateText}>
              There are zero matches. Use the form to search for People or Movies.
            </Text>
          )}
          <MovieList title="Movie List" movieList={searchResults} />
          <View style={{ borderTopWidth: 1, borderTopColor: '#ccc', paddingTop: 30 }}>
            <CustomButton
              onPress={() => navigation.navigate('Search')}
            >
              BACK TO SEARCH
            </CustomButton>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  zeroStateText: {
    color: '#999',
    ...text.regular,
  }
});
