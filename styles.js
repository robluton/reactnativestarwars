import { StyleSheet } from 'react-native';
import * as theme from './theme';

const layout = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    marginVertical: 30,
  },
});
const text = StyleSheet.create({
  regular: {
    fontFamily: 'montserrat',
  },
  semiBold: {
    fontFamily: 'montserrat-semi-bold',
  }
});

const textColor = StyleSheet.create({
  light: {
    color: '#999999'
  },
  medium: {
    color: '#333333',
  }
});

const colors = StyleSheet.create({
  primary: { color: theme.colors.primary },
});

const backgroundColors = StyleSheet.create({
  primary: { backgroundColor: theme.colors.primary }
});

export { layout, text, textColor, colors, backgroundColors };